<?php

class Profile extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $firstname;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $lastname;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $img;

    /**
     *
     * @var integer
     * @Column(type="integer", length=9, nullable=true)
     */
    public $student_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $faculty;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $major;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("uniapp");
        $this->setSource("profile");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'profile';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Profile[]|Profile|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Profile|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
