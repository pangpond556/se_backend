<?php

class UserController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }

    public function adduserAction()
    {
    	if($this->request->isPost()){
    		/*$user = new User();
    		$email = $this->request->getPost("email");
    		$password = $this->request->getPost("password");
    		$user->email = $email;
    		$user->password = $this->security->hash($password);
            $user->user_status = 1;
    		$user->save();
            $profile = new Profile();
            $profile->user_id = $user->id;
            $profile->save();
            */
        $email = $this->request->getPost("email");
        $password = $this->security->hash($this->request->getPost("password"));
        $student_id = $this->request->getPost("student_id");
        $username = $this->request->getPost("username");
        $postdata = array('email' => urlencode($email),
                          'password' => urlencode($password),
                          'student_id' => urlencode($student_id),
                          'username' => urlencode($username));
        foreach($postdata as $key => $value) {
          $postdata_string .= $key.'='.$value.'&';
        }
        rtrim($postdata_string, '&');
        $ch = curl_init("http://chromauniapp.ddns.net:3000/user/add");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
       $this->view->setVars(["result" => $result]);

    }

  }
  public function loginAction(){
    if($this->request->isPost()){
      $email = $this->request->getPost("email");
      $password = $this->request->getPost("password");
      $postdata = array('email' => urlencode($email),
                        'password' => urlencode($password));
      foreach($postdata as $key => $value) {
        $postdata_string .= $key.'='.$value.'&';
      }
      rtrim($postdata_string, '&');
      $ch = curl_init("http://chromauniapp.ddns.net:3000/user/login");
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result = curl_exec($ch);
      if ($this->security->checkHash($password, $result)) {
                  echo "done";
              }else{echo "string";}
    //  echo $result;
      exit();
      //$this->view->setVars(["result" => $result]);

    }
  }
}
