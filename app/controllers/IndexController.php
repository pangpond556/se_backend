<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, "http://chromauniapp.ddns.net:3000/news");
      curl_setopt($curl, CURLOPT_POST, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      $result = curl_exec($curl);
      curl_close($curl);
      $result_arr = json_decode($result, true);
      $this->view->setVars(["news" => $result_arr]);
    }

    public function testAction()
    {
    	if($this->request->isPost()) {
    		$title = $this->request->getPost("title");
        	$detail = $this->request->getPost("details");
    	    $category = $this->request->getPost("category");
        	$tag = $this->request->getPost("tag");
        	$postdata = array('title' => urlencode($title),
        					'detail' => urlencode($detail),
        					'category' => urlencode($category),
        					'tag' => urlencode($tag));
        	foreach ($postdata as $key => $value) {
        		$postdata_string .= $key.'='.$value.'&';
        	}
        	rtrim($postdata_string, '&');
        	$ch = curl_init("http://chromauniapp.ddns.net:3000/news/add");
        	curl_setopt($ch, CURLOPT_POST, true);
        	curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata_string);
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        	$result = curl_exec($ch);
        	$this->view->setVars(["result" => $result]);
    	}
    }

}
